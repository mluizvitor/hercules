import mongoose from "mongoose";

const PlanoSchema = new mongoose.Schema({
  user: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  exercicios: [
    {
      title: {
        type: String,
        required: true
      },
      serie: Number,
      repeticao: Number,
      tempo: Number,
      uni_tempo: {
        type: String,
        enum: ["minutos", "segundos"],
        required: function() {
          return this.tempo != null;
        }
      }
    }
  ]
});

export default mongoose.model("planos", PlanoSchema);
