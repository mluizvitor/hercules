import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';

const User = new mongoose.Schema({
  username: {
    type: String,
    required: false,
    unique: true,
    lowercase: true,
  },
  nome: {
    type: String,
    required: true,
    trim: true,
  },
  nascimento: Date,
  objetivo: {
    type: String,
    enum: ['GMM', 'QV', 'Emagrecer']
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
  }
});

User.pre('save', async function (next){
  const user = this;

  if(user.isModified('password')){
    user.password = await bcrypt.hash(user.password, 8)
  }

  next();
});

export default mongoose.model("user", User);