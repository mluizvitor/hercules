import { parseISO, addMonths } from "date-fns";
import Plano from "../model/PlanoSchema";

class PlanoController {
  async index(req, res) {
    const planos = await Plano.find({user: req.userId}, {user: 1, title: 1, exercicios: 1}).sort({title: 'asc'});

    return res.json(planos);
  }

  async store(req, res) {
    const user = req.userId;
    const { title, exercicios } = req.body;

    const planExists = await Plano.findOne({
      user,
      title
    });

    if (planExists) {
      return res.status(400).json({ error: "Escolha outro nome para o plano" });
    }

    const plano = await Plano.create({ user, title, exercicios });

    return res.json(plano);
  }

  async delete(req, res) {
    const plano = await Plano.findByIdAndRemove(req.params.id);

    if(!plano) {
      return res.status(400).json({error: 'Este plano não existe'})
    }

    return res.json({success: 'Plano deletado com sucesso'});
  }
}

export default new PlanoController();
