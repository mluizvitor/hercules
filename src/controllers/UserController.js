import bcrypt from 'bcryptjs';

import User from '../model/User';

class UserController {
  async store(req, res) {
    const {username} = req.body;

    const user_exists = await User.findOne({username});

    if(user_exists){
      return res.status(401).json({error: "Este nome de usuário já está em uso."})
    }

    const new_user = await User.create(req.body);

    return res.json(new_user);
  }
}

export default new UserController();