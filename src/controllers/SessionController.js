import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

import User from "../model/User";

class SessionController {
  async store(req, res) {
    const { username, password } = req.body;

    const user = await User.findOne({ username });

    if (!user) {
      return res.status(401).json({ error: "Usuário ou senha incorretos" });
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(401).json({ error: "Usuário ou senha incorretos" });
    }

    const token = jwt.sign({ _id: user._id }, process.env.JWT_KEY, {
      expiresIn: "30d"
    });

    await user.save();

    return res.json({ user, token });
  }
}

export default new SessionController();
