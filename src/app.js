import express from 'express';
import routes from './routes';
import mongoose from 'mongoose';

const app = express();

mongoose.set('useCreateIndex', true);
mongoose.Promise = global.Promise;
mongoose.connect(
  process.env.MONGODB_URL,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

app.use(express.json());
app.use(routes);

export default app;