import { Router } from 'express';
import PlanoController from './controllers/PlanoController';
import UserController from './controllers/UserController';
import SessionController from './controllers/SessionController';
import authMiddleware from './middleware/auth';

const routes = Router();


routes.post('/users', UserController.store);
routes.post('/login', SessionController.store);

routes.use(authMiddleware);

routes.get('/planos', PlanoController.index);
routes.post('/planos', PlanoController.store);
routes.delete('/planos/:id', PlanoController.delete);

export default routes;